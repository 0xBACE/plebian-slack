import os
import time
import re
from slackclient import SlackClient
import logging
import stat
from subprocess import check_output as co
logging.basicConfig(level=logging.DEBUG, format='%(relativeCreated)6d %(threadName)s %(message)s')




client = SlackClient('xoxb-key')

starterbot_id = None


RTM_READ_DELAY = 1
EXAMPLE_COMMAND = 'do'
MENTION_REGEX = "^<@(|[WU].+?)>(.*)"


def parse_bot_commands(slack_events):
    for event in slack_events:
        if event["type"] == "message" and not "subtype" in event:
            user_id, message = parse_direct_mention(event["text"])
            if user_id == bot_id:
                return message, event["channel"]
    return None, None

def parse_direct_mention(message_text):
    matches = re.search(MENTION_REGEX, message_text)
    # the first group contains the username, the second group contains the remaining message
    return (matches.group(1), matches.group(2).strip()) if matches else (None, None)

def handle_command(command, channel):
    logging.debug('command: {}, channel: {}'.format(command, channel))
    # Default response is help text for the user
    default_response = "Not sure what you mean. Try help."

    # Finds and executes the given command, filling in response
    response = None
    
    arg = command
    
    
    search = arg.split(' ')[0]
    check = arg.split(' ')
    server = channel
    ser = channel
    if len(check) > 1:
        args = arg.split(' ', 1)[1]
        if os.path.exists('scripts/' + search):#checks if the plz script exist
            slashorno = co(['./scripts/' + search, args]).decode('utf-8') #parse the input so it can detect /
            if slashorno.startswith("/"): #if there is a slash
                slasharg = slashorno[1:] #strips away not needed chars|
                slashfinal = slasharg[:-1]#strips away not needed chars
                response = str(co(['./scripts/' + slashfinal]).decode('utf-8')) #does the script that included slash
            else:
                str(co(['./scripts/' + search, args]).decode('utf-8')) # if it dosent find / it will run just the script
        elif os.path.exists('userscripts/' + ser + "/" + search):#checks if the plz script is in userscripts
            slashorno = co(['./userscripts/' + ser + "/" + search, args]).decode('utf-8')  #parse the input so it can detect /
            if slashorno.startswith("/"): #if there is a slash
                slasharg = slashorno[1:]#strips away not needed chars
                slashfinal = slasharg[:-1]#strips away not needed chars
                response = str(co(['./scripts/' + slashfinal]).decode('utf-8'))#does the script that included slash
            else:
                response = str(co(['./userscripts/' + ser + "/" + search, args]).decode('utf-8'))# if it dosent find / it will run just the script
        elif arg.startswith("make"):#checks for plz make
            nomake = arg[5:]#removes make from plz make
            makename = nomake.split(' ')[0]#parseing
            if os.path.exists('scripts/' + makename):#check if you are trying to overwrite scripts
                response = ("exist! you cant overwrite main scripts!")
            else:
                folder = channel
                if not os.path.exists('userscripts/' + folder):
                    os.makedirs('userscripts/' + folder)
                    file = open("userscripts/" + folder + "/" + makename, 'w+') #make userscript
                    makecommand = nomake.split(" ", 1)[1]
                    file.write('#!/usr/bin/python3')
                    file.write("\n")
                    file.write('print("""' + makecommand + '""")')#uses the simple print("""text here""")
                    response = str("Command made! use: @IOsc " + makename)
                    file.close()
                    st = os.stat('userscripts/' + folder + "/" + makename)
                    os.chmod('userscripts/' + folder + "/" + makename, st.st_mode | stat.S_IEXEC)#chmod because damn thing makes unrunable programms
                else:
                    file = open("userscripts/" + folder + "/" + makename, 'w+') #make userscript
                    makecommand = nomake.split(" ", 1)[1]
                    file.write('#!/usr/bin/python3')
                    file.write("\n")
                    file.write('print("""' + makecommand + '""")')#uses the simple print("""text here""")
                    response = str("Command made! use: @IOsc " + makename)
                    file.close()
                    st = os.stat('userscripts/' + folder + "/" + makename)
                    os.chmod('userscripts/' + folder + "/" + makename, st.st_mode | stat.S_IEXEC)#chmod because damn thing makes unrunable programms
        elif arg.startswith("delete"): #checks for delete
                filenamedel = arg[7:]#stripts delete
                if os.path.exists('scripts/' + filenamedel):#check if it exist in scripts
                    response = str("you cant delete main scripts!") # you cant delete main scripts
                elif not os.path.exists("userscripts/" + server + "/" + filenamedel):
                    response = str("cant find script!")
                else:
                    os.remove("userscripts/" + server + "/" + filenamedel) # else: just remove the script
                    response = str("Deleted") #deleted

        else:
            response = str("script does not exist!") # if it dosent exist after plz, dosent exist
    else:
        if arg.startswith("help"):
           response = str(co(['./scripts/help', str(server)]).decode('utf-8'))
        elif os.path.exists('scripts/' + search):#checks if the plz script exist
            slashorno = co(['./scripts/' + search]).decode('utf-8') #parse the input so it can detect /
            if slashorno.startswith("/"): #if there is a slash
                slasharg = slashorno[1:] #strips away not needed chars
                slashfinal = slasharg[:-1]#strips away not needed chars
                response = str(co(['./scripts/' + slashfinal]).decode('utf-8')) #does the script that included slash
            else:
                response = str(co(['./scripts/' + search]).decode('utf-8')) # if it dosent find / it will run just the script
        elif os.path.exists('userscripts/' + ser + "/" + search):#checks if the plz script is in userscripts
            slashorno = co(['./userscripts/' + ser + "/" + search]).decode('utf-8')  #parse the input so it can detect /
            if slashorno.startswith("/"): #if there is a slash
                slasharg = slashorno[1:]#strips away not needed chars
                slashfinal = slasharg[:-1]#strips away not needed chars
                response = str(co(['./scripts/' + slashfinal]).decode('utf-8'))#does the script that included slash
            else:
                response = str(co(['./userscripts/' + ser + "/" + search]).decode('utf-8'))# if it dosent find / it will run just the script
        else:
            respone = str("script does not exist!") # if it dosent exist after plz, dosent exist


    
    logging.debug('response: {}'.format(response))
    # Sends the response back to the channel
    client.api_call(
        "chat.postMessage",
        channel=channel,
        text=response or default_response
    )






if __name__ in '__main__':
    if client.rtm_connect(with_team_state=False):
        logging.info("Bot connected and running!")
        # Read bot's user ID by calling Web API method `auth.test`
        bot_id = client.api_call("auth.test")["user_id"]
        while True:
            command, channel = parse_bot_commands(client.rtm_read())
            if command:
                handle_command(command, channel)
            time.sleep(RTM_READ_DELAY)
    else:
        print("Connection failed. Exception traceback printed above.")


    
